@extends('layouts.master')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Configuracion Personal</h3>
                <div class="btn-group btn-group-xs pull-right">
                    <button class="btn btn-default btn-flat btn-xs collapsed" type="button" data-toggle="collapse" data-target="#filter"><i class="fa fa-filter"></i> @lang('task.text.buttons.filter')</button>
                    <a href="{!! route('tasks.create') !!}" class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> @lang('task.text.buttons.add_task')</a>
                </div>
            </div>
            <div class="box-body table-responsive">

            </div>
git st
        </div>
    </div>
</div>
<div class="modal fade" id="task-modal">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@stop