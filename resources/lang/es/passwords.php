<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos seis caracteres y combinar la confirmación.',
    'user' => "No podemos encontrar un usuario con ese e-mail..",
    'token' => 'Este token de restablecimiento de contraseña no es válida.',
    'sent' => 'Hemos enviado un correo electrónico de su enlace de restablecimiento de contraseña!',
    'reset' => 'Tu contraseña ha sido restablecida!',

];
