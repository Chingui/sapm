<?php

namespace SAPM\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClientRepository
 * @package namespace SAPM\Repositories;
 */
interface ClientRepository extends RepositoryInterface
{
    //
}
